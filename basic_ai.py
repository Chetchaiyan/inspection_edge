from base_ai_tf2 import *

class MyCNNInferencer(BaseTensorFlowAI):
    input_size = (224, 224)

    def _process_output(self, output_dict):
        return output_dict['softmax'][0]

class MyCNNBinaryInferencerKeras(BaseTensorKerasAI):
    input_size = (224, 224)

    def _preprocess_input(self, input):
        return tf.keras.applications.mobilenet_v2.preprocess_input(input)

    def _process_output(self, pred):
        return pred
