import numpy as np
from basic_ai import MyCNNInferencer

zero_image = np.ones([1, 224, 224, 3], dtype=np.float32)
inferencer = MyCNNInferencer(['cnn_input'], ['softmax'])
inferencer.create_graph('cap_mobilenet/freezed_model.pb')
result = inferencer.inference( {'cnn_input' :zero_image} )
print(result)

# Test model with zeros array
# Test Inference from freezed_graph
import numpy as np
import tensorflow as tf

frozen_graph="cap_mobilenet/freezed_model.pb"
with tf.gfile.GFile(frozen_graph, "rb") as f:
    restored_graph_def = tf.GraphDef()
    restored_graph_def.ParseFromString(f.read())

with tf.Graph().as_default() as graph:
    cnn_input, softmax = tf.import_graph_def( restored_graph_def, input_map=None,
                 return_elements=['cnn_input:0', 'softmax:0'], name="")

sess= tf.Session(graph=graph)
feed_dict_testing = {cnn_input: np.ones( (1, 224, 224, 3), dtype=np.float32 )}
result=sess.run(softmax, feed_dict=feed_dict_testing)
print(result)
