import socket
import asyncore
import numpy as np
import cv2

class BaseAutoInference:
    def __init__(self, master, refresh_source_fn, inference_fn):
        self.master = master
        self.refresh_source_fn = refresh_source_fn
        self.inference_fn = inference_fn

    def start(self):
        pass

    def stop(self):
        pass

    def auto_inference_frame(self, frame_image):
        return frame_image

class TimerAutoInference(BaseAutoInference):
    def __init__(self, master, refresh_source_fn, inference_fn, interval):
        super().__init__(master, refresh_source_fn, inference_fn)
        self.interval = max(interval, 50)
        self.job = None

    def start(self):
        super().start()
        self.job = self.master.after(self.interval, self.run)

    def run(self):
        # ToDo : Make the timer more precise, reduce the time of inference
        self.refresh_source_fn()
        self.inference_fn()
        self.job = self.master.after(self.interval, self.run)

    def stop(self):
        super().stop()
        self.master.after_cancel(self.job)

TCP_IP = '127.0.0.1'
TCP_PORT = 3737

import threading

class EchoHandler(asyncore.dispatcher_with_send):

    def __init__(self, sock, auto_obj):
        super().__init__(sock)
        self.auto_obj = auto_obj

    def handle_read(self):
        data = self.recv(8192).decode('utf-8').strip()
        if data == 'i' :
            self.auto_obj.run()

class TcpServer(asyncore.dispatcher):
    def __init__(self, host, port, auto_obj):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)
        self.auto_obj = auto_obj

    def handle_accept(self):
        pair = self.accept()
        if pair is not None :
            sock, addr = pair
            print('Incoming connection from {}'.format(repr(addr)))
            echo = EchoHandler(sock, self.auto_obj)

class TcpAutoInference(BaseAutoInference):
    def __init__(self, master, refresh_source_fn, inference_fn):
        super().__init__(master, refresh_source_fn, inference_fn)

    def start(self):
        super().start()
        self.server = TcpServer(TCP_IP, TCP_PORT, self)
        self.t = threading.Thread(target=asyncore.loop)
        self.t.start()

    def run(self):
        self.refresh_source_fn()
        self.inference_fn()

    def stop(self):
        super().stop()
        print("TCP Stop Function")
        asyncore.close_all()
        # asyncore.ExitNow('Stop TCP Auto Inference.')
        self.server.close()
        self.t.join(0.1)

from license_plate_recognition_ai import LicensePlateDetectionAI

class ObjectDetectionAutoInference(BaseAutoInference):
    def __init__(self, master, refresh_source_fn, inference_fn):
        super().__init__(master, refresh_source_fn, inference_fn)
        self.detection_ai = LicensePlateDetectionAI(input_tensor=['image_tensor'],
                    output_tensor=['num_detections', 'detection_boxes', 'detection_scores', 'detection_classes'])
        self.detection_ai.create_graph('license_plate_detector_ssd_mobilenet/frozen_inference_graph.pb')

    def auto_inference_frame(self, frame_image):
        # ToDo : Proceed object detection here, then draw rect on detected item.
        detection = self.detection_ai.inference({'image_tensor': np.expand_dims(frame_image, axis=0)})
        if detection :
            for i in detection :
                # ToDo : crop image before send to full infrence
                crop_image, resize_image = self._detection_to_keypoint(frame_image, i)
                # ToDo : proceed full inference as inference_fn
                self.inference_fn(crop_image)
                # Note : refresh_source_fn isn't use since source is refresh by itself and call auto_inference_frame if found
        else :
            pass
        return frame_image

    def _detection_to_keypoint(self, image, detection_result, output_size=(224, 224) ):
        keypoint = detection_result['box']
        PAD = 2
        h, w = image.shape[:2]
        point_1 = (int(keypoint[1] * w - PAD), int(keypoint[0] * h - PAD))
        point_2 = (int(keypoint[3] * w + PAD), int(keypoint[2] * h + PAD))
        crop_image = image[point_1[1]:point_2[1],point_1[0]:point_2[0],:].copy()
        return crop_image, cv2.resize(crop_image, output_size)

    def start(self):
        super().start()

    def stop(self):
        super().stop()
