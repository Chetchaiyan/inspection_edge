from tkinter import *
import cv2
from PIL import Image, ImageTk
import numpy as np
import chet_helper as helper
from base_ai_tf2 import *
import string
import tensorflow as tf
import time

class OCRResultUI(Frame):

    def __init__(self, result, image, master=None):
        super().__init__(master, borderwidth=1, relief="solid")
        self.result = result
        self.image = image
        self.create_widget()

    def create_widget(self):
        img = helper.fit_image(self.image, (128, 128))
        h, w = img.shape[:2]
        b_x = int((128 - w) / 2)
        b_y = int((128 - h) / 2)
        img = cv2.copyMakeBorder( img, b_y, b_y, b_x, b_x, cv2.BORDER_CONSTANT, (0, 0, 0))
        img = Image.fromarray(img)
        i = ImageTk.PhotoImage(img)
        self.label_image = Label(self, image=i)
        self.label_image.image = i
        self.label_image.grid(row=0, column=0, rowspan=2, sticky=N+E+W, padx=5, pady=5)
        self.label_text = StringVar()
        self.result_text = StringVar()
        self.result_text.set("{}".format(str(self.result)))
        self.label_1 = Label(self, textvariable=self.label_text, bg="#aaa", fg="#000", font=(None, 28))
        self.label_1.grid(row=0, column=1, sticky=N+E+W, padx=5, pady=5, ipadx=20, ipady=10)
        self.label_1 = Label(self, textvariable=self.result_text)
        self.label_1.grid(row=1, column=1, padx=5, pady=5)

class OcrAI(BaseComplexModel):
    input_size = (448, 48)
    WORD_LIST = list(string.ascii_lowercase + string.punctuation + string.digits) + ['<pad>']

    def __init__(self, input_tensor=[], output_tensor=[]):
        pass

    def load_model(self, forzen_graph_file=''):
        self.loaded_c_layer = tf.saved_model.load('ocr_model/convolutional_layer')
        self.loaded_r_layer = tf.saved_model.load('ocr_model/recurrent_layer')
        self.loaded_t_layer = tf.saved_model.load('ocr_model/transcription_layer')
        self.c_layer = self.loaded_c_layer.signatures['serving_default']
        self.r_layer = self.loaded_r_layer.signatures['serving_default']
        self.t_layer = self.loaded_t_layer.signatures['serving_default']

    def inference(self, input):
        start_time = time.time()
        img = np.squeeze(input, axis=0)
        img = tf.cast(img, tf.float32)
        img = tf.keras.applications.inception_v3.preprocess_input(img)
        img = tf.expand_dims(img,  0)
        features = self.c_layer(img)
        x = self.r_layer(features['output_0'])
        x = self.t_layer(x['output_0'])
        x = x['output_0']
        y_pred = x[:, 2:, :]
        y_pred = tf.math.argmax(y_pred, axis=2)
        word = self.pred_to_word(y_pred[0])
        elapsed_time = time.time() - start_time
        print("elapsed time : {:.4f} s".format(elapsed_time))
        return word

    def pred_to_word(self,pred):
        remove_duplicate = []
        previous = -1
        for each_pred_char in pred :
            if previous != each_pred_char :
                remove_duplicate.append(each_pred_char)
            previous = each_pred_char
        word = ''
        for each_char in remove_duplicate :
            if self.WORD_LIST[each_char] != '<pad>' :
                word += self.WORD_LIST[each_char]
        return word

    def destroy(self):
        pass
