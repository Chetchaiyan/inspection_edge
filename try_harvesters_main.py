import numpy as np
import cv2

from harvesters.core import Harvester
from harvesters.util import pfnc


def main() :
        with Harvester() as h :
                h.add_cti_file('C:/Program Files/Baumer GAPI SDK/Components/Bin/x64/bgapi2_gige.cti')
                h.update_device_info_list()
                print( len(h.device_info_list) )
                print( h.device_info_list[0] )
                with h.create_image_acquirer(0) as ia :
                        ia.device.node_map.Width.value, ia.device.node_map.Height.value = 2592  , 1944
                        ia.device.node_map.PixelFormat.value = 'BayerRG8'
                        print(dir(ia.device.node_map))
                        ia.start_image_acquisition()
                        with ia.fetch_buffer() as buffer:
                                print(buffer)
                                component = buffer.payload.components[0]
                                _1d = component.data
                                width = component.width
                                height = component.height
                                data_format = component.data_format
                                content = component.data.reshape(height, width)
                                content = cv2.demosaicing(content, cv2.COLOR_BAYER_BG2RGB, 0)
                                cv2.imwrite('color_img.jpg', content)
                                cv2.imshow("image", content)
                                cv2.waitKey()


if __name__ == '__main__' :
    main()