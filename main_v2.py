import cv2
import io
import numpy as np
import requests
import tensorflow as tf
import argparse
import os
import re

from tkinter import *
from tkinter import filedialog, messagebox
from PIL import Image, ImageTk
from harvesters.core import Harvester

class MyCNNInferencer :
    input_size = (224, 224)

    def __init__(self, f_graph_file):
        with tf.gfile.GFile(f_graph_file, "rb") as f:
            restored_graph_def = tf.GraphDef()
            restored_graph_def.ParseFromString(f.read())
        with tf.Graph().as_default() as self.graph:
            self.cnn_input, self.softmax = tf.import_graph_def( restored_graph_def, input_map=None,
                         return_elements=['cnn_input:0', 'softmax:0'], name="")

    def inference(self, image):
        sess = tf.Session(graph=self.graph)
        feed_dict_testing = { self.cnn_input: image }
        result = sess.run(self.softmax, feed_dict=feed_dict_testing)
        return result

class ResultFrame(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.create_widget()

    def create_widget(self):
        self.label_text = StringVar()
        self.label_1 = Label(self, textvariable=self.label_text, bg="#3F3", fg="#000", font=(None, 32))
        b_inference = Button(self, text="Back", command=self.master.do_capture)
        self.label_1.grid(row=0, column=0, sticky=N+E+W, padx=5, pady=5, ipadx=40, ipady=10)
        b_inference.grid(row=1, column=0, sticky=E+W, padx=5, pady=5, ipadx=10, ipady=5)
        self.grid_rowconfigure(0, weight=1)

    def set_result(self, is_pass, text="PASS"):
        if is_pass :
            self.label_1.config(bg="#3F3")
        else :
            self.label_1.config(bg="#F33")
        self.label_text.set(text)

class ReviewFrame(Frame):

    def __init__(self, width, height, master=None):
        super().__init__(master)
        self.canvas_width = width
        self.canvas_height = height
        self.create_widget()

    def create_widget(self):
        self.canvas = Canvas(self, width=self.canvas_width, height=self.canvas_height)
        self.canvas.create_rectangle(0, 0, self.canvas_width, self.canvas_height, fill="black")
        self.canvas.pack()

    def set_image(self, cv2_image):
        image = Image.fromarray(cv2_image)
        image = ImageTk.PhotoImage(image)
        self.image = image
        self.canvas.create_image(self.canvas_width / 2, self.canvas_height / 2, image=self.image)

class SideFrame(Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.create_widget()

    def create_widget(self):
        label_title_crop = Label(self, text="Crop", font=(None, 20))
        b_inference = Button(self, text="Inference", command=self.master.do_inference)
        label_crop_position = Label(self, text="Position")
        label_crop_size = Label(self, text="Size")
        self.crop_x_var = IntVar()
        self.crop_x_var.trace_variable("w", self.master.update_crop)
        self.crop_y_var = IntVar()
        self.crop_y_var.trace_variable("w", self.master.update_crop)
        self.crop_w_var = IntVar()
        self.crop_w_var.trace_variable("w", self.master.update_crop)
        self.crop_h_var = IntVar()
        self.crop_h_var.trace_variable("w", self.master.update_crop)
        crop_x = Entry(self, textvariable=self.crop_x_var, width=5)
        crop_y = Entry(self, textvariable=self.crop_y_var, width=5)
        crop_w = Entry(self, textvariable=self.crop_w_var, width=5)
        crop_h = Entry(self, textvariable=self.crop_h_var, width=5)
        label_title_crop.grid(row=0, column=0, columnspan=3, sticky=N+W, padx=5, pady=5)
        label_crop_position.grid(row=1, column=0, sticky=N+W, padx=5, pady=5)
        crop_x.grid(row=1, column=1)
        crop_y.grid(row=1, column=2)
        label_crop_size.grid(row=2, column=0, sticky=N+W, padx=5, pady=5)
        crop_w.grid(row=2, column=1)
        crop_h.grid(row=2, column=2)
        b_inference.grid(row=3, column=0, columnspan=3, sticky=S+E+W, padx=5, pady=5, ipadx=10, ipady=5)
        self.grid_rowconfigure(3, weight=1)

    def get_crop(self):
        try :
            x = int(self.crop_x_var.get())
            y = int(self.crop_y_var.get())
            w = int(self.crop_w_var.get())
            h = int(self.crop_h_var.get())
            w = 128 if w < 8 else w
            h = 128 if h < 8 else h
            return ((x,y), (w,h))
        except :
            return ((0, 0), (128, 128))

class SourceFrame(Frame):
    def __init__(self, width, height, master=None):
        super().__init__(master)
        self.webcam_job = None
        self.canvas_width = width
        self.canvas_height = height
        self.harvester = None
        self.ia = None
        self.crop = ((0, 0), (128, 128))
        self.create_widget()

    def create_widget(self):
        self.canvas = Canvas(self, width=self.canvas_width, height=self.canvas_height)
        self.canvas.create_rectangle(0, 0, self.canvas_width, self.canvas_height, fill="black")
        v = StringVar()
        v.set("a default value")
        e = Entry(self, textvariable=v)
        b_link = Button(self, text="Link")
        b_webcam = Button(self, text="Web Cam", command=self.webcam_mode)
        b_baumer = Button(self, text="Baumer EXG50c", command=self.harvesters_mode)
        b_browse = Button(self, text="Browse Image", command=self.local_file_mode)
        b_auto_file = Button(self, text="Auto Image", command=self.auto_local_file_mode)
        self.canvas.pack()
        e.pack(fill=BOTH, expand=True, side=LEFT)
        b_link.pack(side=LEFT)
        b_webcam.pack(side=LEFT)
        b_baumer.pack(side=LEFT)
        b_browse.pack(side=LEFT)
        b_auto_file.pack(side=LEFT)

    def _destroy_harvesters(self):
        if self.ia is not None :
            self.ia.stop_image_acquisition()
            self.ia.destroy()
            self.ia = None
        if self.harvester is not None :
            self.harvester.reset()
            self.harvester = None

    def on_closing(self):
        print("Closing")
        self._destroy_harvesters()

    def for_inference(self):
        return self.cv2_image[self.crop[0][1]:self.crop[0][1]+self.crop[1][1],
                    self.crop[0][0]:self.crop[0][0]+self.crop[1][0], :]

    def set_crop(self, crop):
        self.crop = crop

    def harvesters_mode(self):
        self._destroy_harvesters()
        self.harvester = Harvester()
        self.harvester.add_cti_file('C:/Program Files/Baumer GAPI SDK/Components/Bin/x64/bgapi2_gige.cti')
        self.ia = self.harvester.create_image_acquirer(0)
        self.ia.device.node_map.Width.value, ia.device.node_map.Height.value = 2592  , 1944
        self.ia.device.node_map.PixelFormat.value = 'BayerRG8'
        self.camera_res = (2592, 1944)
        self.ia.start_image_acquisition()
        self._show_harvesters_frame(self.ia, self.canvas)

    def _show_harvesters_frame(self, ia, canvas):
        buffer = ia.fetch_buffer()
        print(buffer)
        component = buffer.payload.components[0]
        content = component.data.reshape(component.height, component.width)
        self.cv2_image = cv2.demosaicing(content, cv2.COLOR_BAYER_BG2RGB, 0)
        copy_image = self.cv2_image.copy()
        cv2.rectangle(copy_image,(self.crop[0][0],self.crop[0][1]),
                    (self.crop[0][0]+self.crop[1][0],self.crop[0][1]+self.crop[1][1]),(0,255,0),3)
        copy_image = cv2.resize(copy_image, (self.canvas_width, self.canvas_height) )
        buffer.queue()
        cv2.imwrite('/tmp/temp.jpg', copy_image)
        img = Image.fromarray(copy_image)
        self.image = ImageTk.PhotoImage(img)
        self.canvas.create_image(self.canvas_width / 2, self.canvas_height / 2, image=self.image)
        if self.webcam_job is not None :
            canvas.after_cancel(self.webcam_job)
        self.webcam_job = canvas.after(5,
                    lambda ia=ia, canvas=canvas, f=self: f._show_harvesters_frame(ia, canvas) )

    def webcam_mode(self):
        self.webcam = cv2.VideoCapture('/dev/video0')
        self.camera_res = self._set_webcam_res(self.webcam, 1980, 1080)
        self._show_webcam_frame(self.webcam, self.canvas)

    def local_file_mode(self, selected_file=None):
        if selected_file is None :
            selected_file = filedialog.askopenfilename(filetypes=[("image files","*.jpg *.jpeg *.bmp")] )
        with open(selected_file, 'rb') as f :
            image_content = f.read()
        cv_image = cv2.imread(selected_file,cv2.IMREAD_COLOR)
        self.cv2_image = cv2.cvtColor(cv_image,cv2.COLOR_BGR2RGB)
        copy_image = self.cv2_image.copy()
        cv2.rectangle(copy_image,(self.crop[0][0],self.crop[0][1]),
                    (self.crop[0][0]+self.crop[1][0],self.crop[0][1]+self.crop[1][1]),(0,255,0),3)
        copy_image = cv2.resize(copy_image, (self.canvas_width, self.canvas_height) )
        image = Image.fromarray( copy_image )
        self.image = ImageTk.PhotoImage(image)
        self.canvas.create_image(self.canvas_width / 2, self.canvas_height / 2, image=self.image)
        self.master.do_inference()

    def auto_local_file_mode(self):
        selected_path = filedialog.askdirectory()
        self.master.auto_file_inference(selected_path)


    def _set_webcam_res(self, cap, x,y):
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, int(x))
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(y))
        return cap.get(cv2.CAP_PROP_FRAME_WIDTH) , cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def _show_webcam_frame(self, cam, canvas):
        _, frame = cam.read()
        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        self.cv2_image = cv2image
        copy_image = self.cv2_image.copy()
        cv2.rectangle(copy_image,(self.crop[0][0],self.crop[0][1]),
                    (self.crop[0][0]+self.crop[1][0],self.crop[0][1]+self.crop[1][1]),(0,255,0),3)
        copy_image = cv2.resize(copy_image, (self.canvas_width, self.canvas_height) )
        img = Image.fromarray(copy_image)
        img.save('/tmp/temp.jpg', 'JPEG')
        self.image = ImageTk.PhotoImage(img)
        self.canvas.create_image(self.canvas_width / 2, self.canvas_height / 2, image=self.image)
        if self.webcam_job is not None :
            canvas.after_cancel(self.webcam_job)
        self.webcam_job = canvas.after(5,
                    lambda cam=cam, canvas=canvas, f=self: f._show_webcam_frame(cam, canvas) )

class MainInterfaceFrame(Frame):

    def __init__(self, width, height, master=None):
        super().__init__(master)
        self.create_widget()
        self.inferencer = MyCNNInferencer("pipe_mobilenet_om.pb")
        self.master.minsize(width, height)
        self.master.maxsize(width, height)

    def create_widget(self):
        self.source = SourceFrame(1000, 750, master=self)
        self.side = SideFrame(master=self)
        self.review = ReviewFrame(1000, 750, master=self)
        self.result = ResultFrame(master=self)
        self._set_inference_frame()

    def _set_review_frame(self):
        self.source.grid_forget()
        self.side.grid_forget()
        self.review.grid(row=0, column=0)
        self.result.grid(row=0, column=1, sticky=N+S)
        self.update()

    def _set_inference_frame(self):
        self.review.grid_forget()
        self.result.grid_forget()
        self.source.grid(row=0, column=0)
        self.side.grid(row=0, column=1, sticky=N+S)
        self.update()

    def update_crop(self, *args):
        self.crop = self.side.get_crop()
        self.source.set_crop( self.crop )

    def do_inference(self):
        self._set_review_frame()
        for_inference = self.source.for_inference()

        # ToDo : Remove print and save, only for debuging
        print(for_inference.shape)
        img = Image.fromarray(for_inference)
        img.save('debug.jpg', 'JPEG')

        for_inference = cv2.resize(for_inference, self.inferencer.input_size)
        for_inference_re = for_inference.reshape([1, self.inferencer.input_size[0], self.inferencer.input_size[1], 3])
        result = self.inferencer.inference( for_inference_re )
        print(result)
        if result[0][0] < 0.5 :
            # ToDo : set_result as result shown
            self.result.set_result(True, "PASS")
        else :
            self.result.set_result(False, "FAIL")
        self.review.set_image(for_inference)

    def do_capture(self):
        self._set_inference_frame()

    def auto_file_inference(self, selected_path):
        selected_file = self._selected_highest_number_file(selected_path)
        self.source.local_file_mode( os.path.join(selected_path, selected_file) )
        self.auto_file_inference_job = self.after(15,
                    lambda s=selected_path, f=self: f.auto_file_inference(s) )

    def _selected_highest_number_file(self, selected_path):
        selected_file = None
        selected_number = 0
        for f in os.listdir(selected_path) :
            re_result = re.findall("\B\d+", f)
            if re_result :
                if int(re_result[0]) > selected_number :
                    selected_number = int(re_result[0])
                    selected_file = f
        return selected_file

tk_root = Tk()
app = MainInterfaceFrame(1280, 790, master=tk_root)

def on_closing():
    app.source.on_closing()
    tk_root.destroy()

def main():
    tk_root.protocol("WM_DELETE_WINDOW", on_closing)
    app.pack()
    app.mainloop()

if __name__ == "__main__" :
    main()
