import numpy as np
import tensorflow as tf

class BaseTensorFlowAI:

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.input_tensor = input_tensor
        self.output_tensor = output_tensor
        self.sess = None

    def create_graph(self, frozen_graph_file):
        self.graph = tf.Graph()
        with self.graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(frozen_graph_file, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')
            self.sess = tf.Session()
            ops = tf.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            self.tensor_dict = {}
            for key in self.output_tensor:
                tensor_name = key + ':0'
                if tensor_name in all_tensor_names:
                    self.tensor_dict[key] = tf.get_default_graph().get_tensor_by_name(tensor_name)

    def destroy(self):
        if self.sess :
            self.sess.close()

    def inference(self, input):
        with self.graph.as_default():
            feed_dict = {}
            for i in self.input_tensor :
                feed_dict[tf.get_default_graph().get_tensor_by_name(i + ':0')] = input[i]
            output_dict = self.sess.run(self.tensor_dict, feed_dict=feed_dict)
        return self._process_output( output_dict )

    def _process_output(self, output_dict):
        return output_dict
