import asyncore, socket

class HTTPClient(asyncore.dispatcher):

    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect( ('127.0.0.1', 3737) )
        self.buffer = bytes('i', 'ascii')

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def handle_read(self):
        print(self.recv(8192))

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        print("handle_write")
        sent = self.send(self.buffer)
        self.buffer = self.buffer[sent:]


client = HTTPClient()
asyncore.loop()
