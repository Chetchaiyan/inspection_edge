from tkinter import *
import cv2
from PIL import Image, ImageTk
import numpy as np
import chet_helper as helper
from base_ai import BaseTensorFlowAI
from license_plate_recognition_ai import LicensePlateKeypointAI, LicensePlateCharacterRecognitionAI
# DetectionAI : use license_plate_recognition_ai.LicensePlateDetectionAI

class LPRKeypointAndRecognitionAI(BaseTensorFlowAI):
    input_size = (224, 224)

    PATH_TO_KEYPOINT_FROZEN_GRAPH = 'lpr_model/license_plate_keypoint_mobilenet/optimized_model.pb'
    PATH_TO_RECOGNITION_FROZEN_GRAPH = 'lpr_model/license_plate_recognition_mobilenet/optimized_model.pb'

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.keypoint_ai = LicensePlateKeypointAI(input_tensor=['cnn_input'],
                    output_tensor=['mtt_regression/BiasAdd'])
        self.recognition_ai = LicensePlateCharacterRecognitionAI(input_tensor=['cnn_input'],
                    output_tensor=['output'])

    def create_graph(self, frozen_graph_file):
        self.recognition_ai.create_graph(self.PATH_TO_RECOGNITION_FROZEN_GRAPH)
        self.keypoint_ai.create_graph(self.PATH_TO_KEYPOINT_FROZEN_GRAPH)

    def destroy(self):
        self.keypoint_ai.destroy()
        self.recognition_ai.destroy()

    def inference(self, input):
        input = input['cnn_input'][0]
        print(input.shape)
        resize_image = self._resize(input)
        keypoint = self.keypoint_ai.inference( {'cnn_input': np.expand_dims(resize_image, axis=0)} )
        wrap_image = self._keypoint_to_recognition(input, keypoint)
        recognition = self.recognition_ai.inference( {'cnn_input': np.expand_dims(wrap_image, axis=0)} )
        result = {
            'keypoint': keypoint,
            'recognition': recognition,
        }
        return result

    def _resize(self, image, output_size=(224, 224) ):
        return cv2.resize(image.copy(), output_size)

    def _keypoint_to_recognition(self, image, keypoint_result, output_size=(224, 224) ):
        pts2 = np.float32( [[0, 0], [output_size[0], 0], [output_size[0], output_size[1]], [0, output_size[1]]] )
        h, w = image.shape[:2]
        keypoint = np.asarray( keypoint_result.copy(), dtype=np.float32 )
        keypoint[:, 0] = keypoint[:, 0] * w
        keypoint[:, 1] = keypoint[:, 1] * h
        M = cv2.getPerspectiveTransform(keypoint, pts2)
        dst = cv2.warpPerspective(image,M,output_size)
        return dst

class LPRResultUI(Frame):

    def __init__(self, result, image, master=None):
        super().__init__(master, borderwidth=1, relief="solid")
        self.result = result
        self.image = image
        self.create_widget()

    def create_widget(self):
        img = helper.fit_image(self.image, (128, 128))
        h, w = img.shape[:2]
        b_x = int((128 - w) / 2)
        b_y = int((128 - h) / 2)
        img = cv2.copyMakeBorder( img, b_y, b_y, b_x, b_x, cv2.BORDER_CONSTANT, (0, 0, 0))
        img = Image.fromarray(img)
        i = ImageTk.PhotoImage(img)
        self.label_image = Label(self, image=i)
        self.label_image.image = i
        self.label_image.grid(row=0, column=0, rowspan=2, sticky=N+E+W, padx=5, pady=5)
        self.label_text = StringVar()
        self.result_text = StringVar()
        self.result_text.set("{}".format(str(self.result['recognition'])))
        self.label_1 = Label(self, textvariable=self.label_text, bg="#aaa", fg="#000", font=(None, 28))
        self.label_1.grid(row=0, column=1, sticky=N+E+W, padx=5, pady=5, ipadx=20, ipady=10)
        self.label_1 = Label(self, textvariable=self.result_text)
        self.label_1.grid(row=1, column=1, padx=5, pady=5)
