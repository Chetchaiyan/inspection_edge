import argparse
import collections
import csv
import cv2
import datetime
import importlib
import io
import json
import numpy as np
import os
import re
import requests
import tensorflow as tf

from tkinter import *
from tkinter import filedialog, messagebox
from PIL import Image, ImageTk
from harvesters.core import Harvester

import chet_helper as helper
from result_recorder import CsvRecorder
from auto_inference import TimerAutoInference, TcpAutoInference, ObjectDetectionAutoInference

from license_plate_recognition_ai import BaseTensorFlowAI
from basic_ai import MyCNNInferencer
from basic_ui import BinaryClassificationResultUI

class ResultFrame(Frame):
    def __init__(self, master=None):
        super().__init__(master, borderwidth=1, relief="solid")
        self.result_ui_list = collections.deque()
        self.max_result = 8
        self.create_widget()

    def create_widget(self):
        pass

    def set_result(self, is_pass, text="PASS"):
        if is_pass :
            self.label_1.config(bg="#3F3")
        else :
            self.label_1.config(bg="#F33")
        self.label_text.set(text)

    def add(self, result_ui):
        self.result_ui_list.appendleft( result_ui )
        if len(self.result_ui_list) > self.max_result :
            earliest_result = self.result_ui_list.pop()
            earliest_result.pack_forget()
            earliest_result.destroy()
        # ToDo : reverse item list
        for i in self.result_ui_list :
            i.pack_forget()
            i.pack(padx=5, pady=5)

class SideFrame(Frame):
    def __init__(self, master=None, config=None):
        super().__init__(master, borderwidth=1, relief="solid")
        self.config = config
        self.create_widget()

    def create_widget(self):
        label_title_crop = Label(self, text="Crop", font=(None, 20))
        self.crop_x_var = IntVar()
        self.crop_x_var.trace_variable("w", self.master.update_crop)
        self.crop_y_var = IntVar()
        self.crop_y_var.trace_variable("w", self.master.update_crop)
        self.crop_w_var = IntVar()
        self.crop_w_var.trace_variable("w", self.master.update_crop)
        self.crop_h_var = IntVar()
        self.crop_h_var.trace_variable("w", self.master.update_crop)
        if 'detector_and_inferencer' in self.config :
            b_detect_and_inference = Button(self, text="Detect & Inference", command=self.master.do_detect_and_inference)
            b_detect_and_inference.grid(row=3, column=0, columnspan=3, sticky=S+E+W, padx=5, pady=5, ipadx=10, ipady=5)
        if 'inferencer' in self.config :
            b_inference = Button(self, text="Inference", command=self.master.do_inference)
            b_inference.grid(row=4, column=0, columnspan=3, sticky=S+E+W, padx=5, pady=5, ipadx=10, ipady=5)
            label_crop_position = Label(self, text="Position")
            label_crop_size = Label(self, text="Size")
            crop_x = Entry(self, textvariable=self.crop_x_var, width=5)
            crop_y = Entry(self, textvariable=self.crop_y_var, width=5)
            crop_w = Entry(self, textvariable=self.crop_w_var, width=5)
            crop_h = Entry(self, textvariable=self.crop_h_var, width=5)
            label_title_crop.grid(row=0, column=0, columnspan=3, sticky=N+W, padx=5, pady=5)
            label_crop_position.grid(row=1, column=0, sticky=N+W, padx=5, pady=5)
            crop_x.grid(row=1, column=1, padx=5, pady=5)
            crop_y.grid(row=1, column=2, padx=5, pady=5)
            label_crop_size.grid(row=2, column=0, sticky=N+W, padx=5, pady=5)
            crop_w.grid(row=2, column=1, padx=5, pady=5)
            crop_h.grid(row=2, column=2, padx=5, pady=5)
        self.grid_rowconfigure(3, weight=1)

    def get_crop(self):
        try :
            x = int(self.crop_x_var.get())
            y = int(self.crop_y_var.get())
            w = int(self.crop_w_var.get())
            h = int(self.crop_h_var.get())
            w = 128 if w < 8 else w
            h = 128 if h < 8 else h
            return ((x,y), (w,h))
        except :
            return ((0, 0), (128, 128))

class SourceFrame(Frame):
    def __init__(self, master=None):
        super().__init__(master, borderwidth=1, relief="solid")
        self.webcam_job = None
        self.harvester = None
        self.ia = None
        self.crop = ((0, 0), (128, 128))
        self.create_widget()
        self.selected_source = None

    def create_widget(self):
        self.canvas = Canvas(self)
        self.v = StringVar()
        self.v.set("a default value")
        e = Entry(self, textvariable=self.v)
        b_link = Button(self, text="Link", command=self.ip_camera_mode)
        b_webcam = Button(self, text="Web Cam", command=self.webcam_mode)
        b_baumer = Button(self, text="Baumer EXG50c", command=self.harvesters_mode)
        b_browse = Button(self, text="Browse Image", command=self.local_file_mode)
        self.canvas.pack(fill=BOTH, expand=True)
        e.pack(fill=BOTH, expand=True, side=LEFT)
        b_link.pack(side=LEFT)
        b_webcam.pack(side=LEFT)
        b_baumer.pack(side=LEFT)
        b_browse.pack(side=LEFT)
        self.update()

    def _destroy_harvesters(self):
        if self.ia is not None :
            self.ia.stop_image_acquisition()
            self.ia.destroy()
            self.ia = None
        if self.harvester is not None :
            self.harvester.reset()
            self.harvester = None

    def on_closing(self):
        print("Closing")
        self._destroy_harvesters()

    def for_detect(self):
        return self.cv2_image

    def for_inference(self):
        return self.cv2_image[self.crop[0][1]:self.crop[0][1]+self.crop[1][1],
                    self.crop[0][0]:self.crop[0][0]+self.crop[1][0], :]

    def full_image(self):
        return self.cv2_image

    def set_crop(self, crop):
        self.crop = crop
        self.refresh_source()

    def harvesters_mode(self):
        self.selected_source = 'harvesters'
        self._destroy_harvesters()
        self.harvester = Harvester()
        self.harvester.add_cti_file('C:/Program Files/Baumer GAPI SDK/Components/Bin/x64/bgapi2_gige.cti')
        self.ia = self.harvester.create_image_acquirer(0)
        self.ia.device.node_map.Width.value, ia.device.node_map.Height.value = 2592  , 1944
        self.ia.device.node_map.PixelFormat.value = 'BayerRG8'
        self.camera_res = (2592, 1944)
        self.ia.start_image_acquisition()
        self._show_harvesters_frame(self.ia, self.canvas)

    def _show_harvesters_frame(self, ia, canvas):
        w = canvas.winfo_width()
        h = canvas.winfo_height()
        buffer = ia.fetch_buffer()
        print(buffer)
        component = buffer.payload.components[0]
        content = component.data.reshape(component.height, component.width)
        self.cv2_image = cv2.demosaicing(content, cv2.COLOR_BAYER_BG2RGB, 0)
        copy_image = self.cv2_image.copy()
        cv2.rectangle(copy_image,(self.crop[0][0],self.crop[0][1]),
                    (self.crop[0][0]+self.crop[1][0],self.crop[0][1]+self.crop[1][1]),(0,255,0),3)
        copy_image = helper.fit_image(copy_image, (self.canvas.winfo_width(), self.canvas.winfo_height()))
        buffer.queue()
        img = Image.fromarray(copy_image)
        self.image = ImageTk.PhotoImage(img)
        self.canvas.create_image(w / 2, h / 2, image=self.image)
        if self.webcam_job is not None :
            canvas.after_cancel(self.webcam_job)
        self.webcam_job = canvas.after(5,
                    lambda ia=ia, canvas=canvas, f=self: f._show_harvesters_frame(ia, canvas) )

    def ip_camera_mode(self):
        self.webcam_mode(self.v.get())

    def webcam_mode(self, url='/dev/video0'):
        self.selected_source = 'webcam'
        self.webcam = cv2.VideoCapture(url)
        self.camera_res = self._set_webcam_res(self.webcam, 1980, 1080)
        self._show_webcam_frame(self.webcam, self.canvas)

    def local_file_mode(self, selected_file=None):
        self.selected_source = 'local_file'
        w = self.canvas.winfo_width()
        h = self.canvas.winfo_height()
        if selected_file is None :
            self.selected_file = filedialog.askopenfilename(filetypes=[("image files","*.jpg *.jpeg *.bmp")] )
        with open(self.selected_file, 'rb') as f :
            image_content = f.read()
        cv_image = cv2.imread(self.selected_file,cv2.IMREAD_COLOR)
        self.cv2_image = cv2.cvtColor(cv_image,cv2.COLOR_BGR2RGB)
        copy_image = self.cv2_image.copy()
        cv2.rectangle(copy_image,(self.crop[0][0],self.crop[0][1]),
                    (self.crop[0][0]+self.crop[1][0],self.crop[0][1]+self.crop[1][1]),(0,255,0),3)
        copy_image = helper.fit_image(copy_image, (self.canvas.winfo_width(), self.canvas.winfo_height()))
        image = Image.fromarray( copy_image )
        self.image = ImageTk.PhotoImage(image)
        self.canvas.create_image(w / 2, h / 2, image=self.image)

    def _set_webcam_res(self, cap, x,y):
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, int(x))
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(y))
        return cap.get(cv2.CAP_PROP_FRAME_WIDTH) , cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def _show_webcam_frame(self, cam, canvas):
        w = canvas.winfo_width()
        h = canvas.winfo_height()
        _, frame = cam.read()
        cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        self.cv2_image = cv2image
        copy_image = self.cv2_image.copy()
        copy_image = self.master.on_auto_inference_frame(copy_image)
        cv2.rectangle(copy_image,(self.crop[0][0],self.crop[0][1]),
                    (self.crop[0][0]+self.crop[1][0],self.crop[0][1]+self.crop[1][1]),(0,255,0),3)
        copy_image = helper.fit_image(copy_image, (self.canvas.winfo_width(), self.canvas.winfo_height()))
        img = Image.fromarray(copy_image)
        self.image = ImageTk.PhotoImage(img)
        self.canvas.create_image(w / 2, h / 2, image=self.image)
        if self.webcam_job is not None :
            canvas.after_cancel(self.webcam_job)
        self.webcam_job = canvas.after(5,
                    lambda cam=cam, canvas=canvas, f=self: f._show_webcam_frame(cam, canvas) )

    # referesh source canvas, actually required for local file only (at this moment) since all camera is refereshed by itself every 5 ms.
    def refresh_source(self) :
        if self.selected_source == 'local_file' :
            self.local_file_mode(selected_file=self.selected_file)

class AboutUsTopLevel(Toplevel):

    def __init__(self, master=None, **options):
        super().__init__(master, options)
        self.wm_title("About Us")
        label_title = Label(self, text='This is about us.', font=(0, 28))
        self.close_btn = Button(self, text="Close", command=self.destroy)
        label_title.grid(row=0, column=0, padx=5, pady=10)
        self.close_btn.grid(row=1, column=0, padx=20, pady=20, ipadx=30, ipady=5)
        self.config(padx=20, pady=10)

class AutoInferenceSetUpTopLevel(Toplevel):
    def __init__(self, master=None, **options):
        super().__init__(master, options)
        self.wm_title("Auto Inference Set Up")

        label_inference_type = Label(self, text="Type")
        choices = ['Timer','tcp/ip','Object Detection']
        self.inference_type_var = StringVar(self)
        self.inference_type_var.set(choices[0])
        inference_type = OptionMenu(self, self.inference_type_var, *choices, command=self.on_type_changed)
        label_inference_type.grid(row=0, column=0)
        inference_type.grid(row=0, column=1, sticky=W+E)
        self.start_btn = Button(self, text="Start", command=self.on_start)
        self.start_btn.grid(row=2, column=0, columnspan=2, padx=20, pady=20, ipadx=30, ipady=5)
        self.grid_columnconfigure(1, weight=1)

        self.timer_frame = Frame(self)
        label_interval = Label(self.timer_frame, text="Interval (ms)")
        self.timer_frame.entry_interval = Entry(self.timer_frame, width=8)
        label_interval.grid(row=1, column=0, padx=5, pady=5)
        self.timer_frame.entry_interval.grid(row=1, column=1, sticky=W, padx=5, pady=5)
        self.timer_frame.grid_columnconfigure(1, weight=1)
        self.timer_frame.config(padx=20, pady=10)
        self.timer_frame.grid(row=1, column=0, columnspan=2)
        self.tcp_frame = Frame(self)
        label_interval = Label(self.tcp_frame, text="This is for tcp/ip detail.")
        label_interval.pack()
        self.od_frame = Frame(self)

    def on_type_changed(self, event):
        self.timer_frame.grid_forget()
        self.tcp_frame.grid_forget()
        self.od_frame.grid_forget()
        if event == 'Timer' :
            self.timer_frame.grid(row=1, column=0, columnspan=2)
        elif event == 'tcp/ip':
            self.tcp_frame.grid(row=1, column=0, columnspan=2)
        elif event == 'Object Detection':
            self.od_frame.grid(row=1, column=0, columnspan=2)

    def on_start(self):
        if self.inference_type_var.get() == 'Timer' :
            try :
                interval = int(self.timer_frame.entry_interval.get())
            except ValueError as e :
                interval = 2000
            config = {
                'timer' : { 'interval' : interval, },
            }
        elif self.inference_type_var.get() == 'tcp/ip' :
            config = {
                'tcp/ip' : {},
            }
        elif self.inference_type_var.get() == 'Object Detection':
            config = {
                'object_detection' : {},
            }
        self.master.start_auto_inference( config )

class MenuBar(Menu):
    pass

class MainInterfaceFrame(Frame):

    def __init__(self, args, master=None):
        super().__init__(master)
        with open(args.config, 'r') as config_file :
            self.config = json.loads(config_file.read())
        self.create_widget()
        if 'inferencer' in self.config :
            module_name, class_name = self.config['inferencer']['class'].rsplit(".", 1)
            inferencer_class = getattr(importlib.import_module(module_name), class_name)
            self.inferencer = inferencer_class(input_tensor=self.config['inferencer']['input_tensor'],
                        output_tensor=self.config['inferencer']['output_tensor'])
            self.inferencer.load_model(self.config['inferencer']['frozen_graph_file'])
        if 'detector_and_inferencer' in self.config :
            d_and_i_cfg = self.config['detector_and_inferencer']
            module_name, class_name = d_and_i_cfg['class'].rsplit(".", 1)
            inferencer_class = getattr(importlib.import_module(module_name), class_name)
            self.detector_and_inferencer = inferencer_class(input_tensor=d_and_i_cfg['input_tensor'],
                        output_tensor=d_and_i_cfg['output_tensor'])
            self.detector_and_inferencer.load_model()
        module_name, class_name = self.config['result_ui']['class'].rsplit(".", 1)
        self.result_ui_class = getattr(importlib.import_module(module_name), class_name)
        print(self.result_ui_class)
        master.minsize(1280, 960)
        self.recording = False
        self.auto_inferencing = False
        self.csv_recorder = CsvRecorder(config=self.config)

    def create_widget(self):
        self.menu_bar = MenuBar(self.master)
        self.menu_bar.add_command(label='Start Auto Inference', command=self.auto_inference)
        self.menu_bar.add_command(label='Start Record', command=self.record)
        self.menu_bar.add_command(label="About Us", command=self.about_us)
        self.master.config(menu=self.menu_bar)
        self.source = SourceFrame(master=self)
        self.side = SideFrame(master=self, config=self.config)
        self.result = ResultFrame(master=self)
        self._set_inference_frame()

    def _set_inference_frame(self):
        self.source.grid(row=0, column=0, sticky=N+S+W+E, padx=5, pady=5)
        self.result.grid(row=0, column=1, sticky=N+S, padx=5, pady=5)
        self.side.grid(row=0, column=2, sticky=N+S, padx=5, pady=5)
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)
        self.update()

    def update_crop(self, *args):
        self.crop = self.side.get_crop()
        self.source.set_crop( self.crop )

    def auto_inference(self):
        if self.auto_inferencing :
            self.menu_bar.entryconfigure('Stop Auto Inference', label='Start Auto Inference')
            self.auto_inference_obj.stop()
            self.auto_inferencing = not self.auto_inferencing
            # Remove status from top of source
        else :
            if self.source.selected_source is not None :
                self.auto_inference_setup_top_level = AutoInferenceSetUpTopLevel(self)

    def on_auto_inference_frame(self, frame_image):
        if self.auto_inferencing :
            return self.auto_inference_obj.auto_inference_frame(frame_image)
        return frame_image

    def start_auto_inference(self, config):
        # Display status at top of source
        print(config)
        if 'timer' in config :
            t_config = config['timer']
            self.menu_bar.entryconfigure('Start Auto Inference', label='Stop Auto Inference')
            self.auto_inference_obj = TimerAutoInference(self, self.source.refresh_source,
                        self.do_inference, t_config['interval'])
            self.auto_inference_obj.start()
            self.auto_inferencing = not self.auto_inferencing
        elif 'tcp/ip' in config :
            self.menu_bar.entryconfigure('Start Auto Inference', label='Stop Auto Inference')
            self.auto_inference_obj = TcpAutoInference(self, self.source.refresh_source, self.do_inference)
            self.auto_inference_obj.start()
            self.auto_inferencing = not self.auto_inferencing
        elif 'object_detection' in config :
            self.menu_bar.entryconfigure('Start Auto Inference', label='Stop Auto Inference')
            self.auto_inference_obj = ObjectDetectionAutoInference(self, self.source.refresh_source, self.do_inference)
            self.auto_inference_obj.start()
            self.auto_inferencing = not self.auto_inferencing
        self.auto_inference_setup_top_level.destroy()

    def record(self):
        if not self.recording :
            self.menu_bar.entryconfigure('Start Record', label='Stop Record')
        else :
            self.menu_bar.entryconfigure('Stop Record', label='Start Record')
        self.recording = not self.recording

    def about_us(self):
        self.about_us_top_level = AboutUsTopLevel(self)

    def do_detect_and_inference(self, image=None):
        if image is not None:
            for_detect = image
        else :
            for_detect = self.source.for_detect()
        print(for_detect.shape)
        # ToDo : fix for support keras
        result, crop = self.detector_and_inferencer.inference( {'cnn_input': for_detect} )
        for r, c in zip(result, crop) :
            self.result.add( self.result_ui_class(r, c, master=self.result) )

    def do_inference(self, image=None):
        if image is not None:
            for_inference = image
        else :
            for_inference = self.source.for_inference()

        # ToDo : Remove print and save, only for debuging
        img = Image.fromarray(for_inference)
        img.save('debug.jpg', 'JPEG')
        for_inference_resize = cv2.resize(for_inference, self.inferencer.input_size)
        for_inference_re = np.expand_dims(for_inference_resize, axis=0)
        result = self.inferencer.inference( for_inference_re )
        self.result.add( self.result_ui_class(result, for_inference, master=self.result) )
        if self.recording :
            self.csv_recorder.record(result, for_inference, self.source.full_image())

    def on_closing(self):
        self.source.on_closing()
        self.master.destroy()

def main():
    parser = argparse.ArgumentParser(prog='Inspection Edge')
    parser.add_argument('--config', metavar='C', nargs='?', default="config.json", help='config files used for setting application.')
    args = parser.parse_args()
    tk_root = Tk()
    app = MainInterfaceFrame(args, master=tk_root)
    tk_root.protocol("WM_DELETE_WINDOW", lambda a=app : a.on_closing() )
    app.pack(fill=BOTH, expand=1)
    app.mainloop()

if __name__ == "__main__" :
    main()
