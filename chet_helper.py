import cv2

def fit_image(image, size):
    c_w, c_h = size[0], size[1]
    i_h, i_w = image.shape[:2]
    if (i_w / c_w) >= (i_h / c_h) :
        new_h = int(i_h * c_w / i_w)
        return cv2.resize(image, (c_w, new_h))
    else :
        new_w = int(i_w * c_h / i_h)
        return cv2.resize(image, (new_w, c_h))
