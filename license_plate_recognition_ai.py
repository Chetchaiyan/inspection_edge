import numpy as np
import tensorflow.compat.v1 as tf_v1
import tensorflow as tf
import cv2
import time
import pickle
import string

from base_ai_tf2 import *

class LicensePlateDetectionAI(BaseTensorFlowAI):

    def _process_output(self, output_dict):
        output = []
        for i in range( int(output_dict['num_detections'][0]) ):
            if output_dict['detection_scores'][0][i] > 0.5 :
                obj = {
                    'score' : output_dict['detection_scores'][0][i],
                    'class' : output_dict['detection_classes'][0][i].astype(np.int64),
                    'box' : output_dict['detection_boxes'][0][i],
                }
                output.append( obj )
            else :
                return output
        return output

class LicensePlateKeypointAI(BaseTensorFlowAI):

    def _process_output(self, output_dict):
        return output_dict['mtt_regression/BiasAdd'][0].reshape((4, 2))

class LicensePlateCharacterRecognitionAI(BaseTensorFlowAI):

    def _process_output(self, output_dict):
        return output_dict['output'][0]

class LicensePlateDigitRecognitionAI(BaseTensorKerasAI):

    def inference(self, input):
        input = tf.keras.applications.mobilenet_v2.preprocess_input(input)
        pred = self.model( input )
        predicted_id = tf.argmax(pred, axis=1).numpy()
        return str(predicted_id[0])

class LicensePlateRecognitionAI(BaseTensorFlowAI):

    PATH_TO_DETECTOR_FROZEN_GRAPH = 'license_plate_detector_ssd_mobilenet/frozen_inference_graph.pb'
    PATH_TO_KEYPOINT_FROZEN_GRAPH = 'license_plate_keypoint_mobilenet/optimized_model.pb'
    PATH_TO_RECOGNITION_FROZEN_GRAPH = 'license_plate_recognition_mobilenet/optimized_model.pb'

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.detection_ai = LicensePlateDetectionAI(input_tensor=['image_tensor'],
                    output_tensor=['num_detections', 'detection_boxes', 'detection_scores', 'detection_classes'])
        self.keypoint_ai = LicensePlateKeypointAI(input_tensor=['cnn_input'],
                    output_tensor=['mtt_regression/BiasAdd'])
        self.recognition_ai = LicensePlateCharacterRecognitionAI(input_tensor=['cnn_input'],
                    output_tensor=['output'])

    def create_graph(self, frozen_graph_file):
        self.detection_ai.create_graph(self.PATH_TO_DETECTOR_FROZEN_GRAPH)
        self.recognition_ai.create_graph(self.PATH_TO_RECOGNITION_FROZEN_GRAPH)
        self.keypoint_ai.create_graph(self.PATH_TO_KEYPOINT_FROZEN_GRAPH)

    def inference(self, input):
        input = input['cnn_input']
        start_time = time.time()
        detection = self.detection_ai.inference({'image_tensor': np.expand_dims(input, axis=0)} )
        elapsed_time = time.time() - start_time
        print("detection time : {:.4f} s".format(elapsed_time))
        result_list = []
        for i in detection :
            crop_image, crop_resize_image = self._detection_to_keypoint(input, i)
            start_time = time.time()
            keypoint = self.keypoint_ai.inference( {'cnn_input': np.expand_dims(crop_resize_image, axis=0)} )
            elapsed_time = time.time() - start_time
            print("keypoint time : {:.4f} s".format(elapsed_time))
            wrap_image = self._keypoint_to_recognition(crop_image, keypoint)
            self.wrap_image = wrap_image
            start_time = time.time()
            recognition = self.recognition_ai.inference( {'cnn_input': np.expand_dims(wrap_image, axis=0)} )
            elapsed_time = time.time() - start_time
            print("recognition time : {:.4f} s".format(elapsed_time))
            result = i.copy()
            result['keypoint'] = keypoint
            result['recognition'] = recognition
            result_list.append( result )
        return result_list

    def destroy(self):
        self.detection_ai.destroy()
        self.keypoint_ai.destroy()
        self.recognition_ai.destroy()

    def _detection_to_keypoint(self, image, detection_result, output_size=(224, 224) ):
        keypoint = detection_result['box']
        PAD = 2
        h, w = image.shape[:2]
        point_1 = (int(keypoint[1] * w - PAD), int(keypoint[0] * h - PAD))
        point_2 = (int(keypoint[3] * w + PAD), int(keypoint[2] * h + PAD))
        crop_image = image[point_1[1]:point_2[1],point_1[0]:point_2[0],:].copy()
        return crop_image, cv2.resize(crop_image, output_size)

    def _keypoint_to_recognition(self, image, keypoint_result, output_size=(224, 224) ):
        pts2 = np.float32( [[0, 0], [output_size[0], 0], [output_size[0], output_size[1]], [0, output_size[1]]] )
        h, w = image.shape[:2]
        keypoint = np.asarray( keypoint_result.copy(), dtype=np.float32 )
        keypoint[:, 0] = keypoint[:, 0] * w
        keypoint[:, 1] = keypoint[:, 1] * h
        M = cv2.getPerspectiveTransform(keypoint, pts2)
        dst = cv2.warpPerspective(image,M,output_size)
        return dst

class LicensePlateRecognitionAIV2(BaseTensorFlowAI):
    PATH_TO_DETECTOR_FROZEN_GRAPH = 'license_plate_detector_ssd_mobilenet/frozen_inference_graph.pb'
    PATH_TO_DIGIT_RECOGNITION_MODEL = [
        'license_plate_recognition_mobilenet_tf2_digit0',
        'license_plate_recognition_mobilenet_tf2_digit1',
        'license_plate_recognition_mobilenet_tf2_digit2',
        'license_plate_recognition_mobilenet_tf2_digit3',
    ]

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.detection_ai = LicensePlateDetectionAI(input_tensor=['image_tensor'],
                    output_tensor=['num_detections', 'detection_boxes', 'detection_scores', 'detection_classes'])
        self.digit_recognition_ai = []
        for i in self.PATH_TO_DIGIT_RECOGNITION_MODEL:
            digit_ai = LicensePlateDigitRecognitionAI(input_tensor=['cnn_input'], output_tensor=['output'])
            self.digit_recognition_ai.append( digit_ai )

    def create_graph(self, frozen_graph_file):
        self.detection_ai.create_graph(self.PATH_TO_DETECTOR_FROZEN_GRAPH)
        for ai, path in zip(self.digit_recognition_ai, self.PATH_TO_DIGIT_RECOGNITION_MODEL) :
            ai.load_model(path)

    def inference(self, input):
        input = input['cnn_input']
        start_time = time.time()
        detection = self.detection_ai.inference({'image_tensor': np.expand_dims(input, axis=0)} )
        elapsed_time = time.time() - start_time
        print("detection time : {:.4f} s".format(elapsed_time))
        result_list = []
        crop_list = []
        for i in detection :
            crop_image, crop_resize_image = self._detection_to_recognition(input, i)
            digit_output = []
            for digit_ai in self.digit_recognition_ai :
                output = digit_ai.inference( np.expand_dims(crop_resize_image, axis=0) )
                digit_output.append( output )
            result = i.copy()
            result['recognition'] = ''.join( digit_output )
            result_list.append( result )
            crop_list.append( crop_image )
        return result_list, crop_list

    def _detection_to_recognition(self, image, detection_result, output_size=(224, 224) ):
        keypoint = detection_result['box']
        PAD = 2
        h, w = image.shape[:2]
        point_1 = (int(keypoint[1] * w - PAD), int(keypoint[0] * h - PAD))
        point_2 = (int(keypoint[3] * w + PAD), int(keypoint[2] * h + PAD))
        crop_image = image[point_1[1]:point_2[1],point_1[0]:point_2[0],:].copy()
        return crop_image, cv2.resize(crop_image, output_size)

class LicensePlateRecognitionAIV3(BaseComplexModel):
    DICT_DATA_FILE = 'lpr_model/license_plate_dict.pickle'
    PATH_TO_DETECTOR_FROZEN_GRAPH = 'lpr_model/license_plate_detector_ssd_mobilenet/frozen_inference_graph.pb'
    PATH_TO_DIGIT_RECOGNITION_MODEL = [
        'lpr_model/license_plate_recognition_mobilenet_tf2_digit0',
        'lpr_model/license_plate_recognition_mobilenet_tf2_digit1',
        'lpr_model/license_plate_recognition_mobilenet_tf2_digit2',
        'lpr_model/license_plate_recognition_mobilenet_tf2_digit3',
        'lpr_model/license_plate_recognition_mobilenet_tf2_char0',
        'lpr_model/license_plate_recognition_mobilenet_tf2_char1',
        'lpr_model/license_plate_recognition_mobilenet_tf2_char2',
        'lpr_model/license_plate_recognition_mobilenet_tf2_province',
    ]

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.detection_ai = LicensePlateDetectionAI(input_tensor=['image_tensor'],
                    output_tensor=['num_detections', 'detection_boxes', 'detection_scores', 'detection_classes'])
        self.digit_recognition_ai = []
        for i in self.PATH_TO_DIGIT_RECOGNITION_MODEL:
            digit_ai = LicensePlateDigitRecognitionAI()
            self.digit_recognition_ai.append( digit_ai )

    def load_model(self):
        self.detection_ai.create_graph(self.PATH_TO_DETECTOR_FROZEN_GRAPH)
        for ai, path in zip(self.digit_recognition_ai, self.PATH_TO_DIGIT_RECOGNITION_MODEL) :
            ai.load_model(path)
        data_dict = pickle.load( open( self.DICT_DATA_FILE , "rb") )
        char_na = '/'
        char_digits = string.digits
        char_char = ''.join(data_dict['char_char'])
        self.province_list = data_dict['province']
        self.char_list = [char_na + char_digits + char_char, char_digits + char_char, char_na + char_char]

    def inference(self, input):
        input = input['cnn_input']
        start_time = time.time()
        detection = self.detection_ai.inference({'image_tensor': np.expand_dims(input, axis=0)} )
        elapsed_time = time.time() - start_time
        print("detection time : {:.4f} s".format(elapsed_time))
        result_list = []
        crop_list = []
        for i in detection :
            crop_image, crop_resize_image = self._detection_to_recognition(input, i)
            digit_output = []
            crop_resize_image_exp = np.expand_dims(crop_resize_image, axis=0)
            for digit_ai in self.digit_recognition_ai :
                output = digit_ai.inference( crop_resize_image_exp )
                digit_output.append( output )
            result = i.copy()
            r_digit, r_char, r_province = self.interpret_result(digit_output)
            result['r_digit'] = r_digit
            result['r_char'] = r_char
            result['r_province'] = r_province
            result['recognition'] = '{} {}\n{}'.format(r_char, r_digit, r_province)
            result_list.append( result )
            crop_list.append( crop_image )
        return result_list, crop_list

    def _process_output(self, pred):
        return pred

    def interpret_result(self, pred):
        r_digit = ''
        r_char = ''
        for idx, p in enumerate(pred):
            if idx <= 3 :
                r_digit += p
            elif idx <= 6 :
                char = self.char_list[idx-4][int(p)]
                if char != '/' :
                    r_char += self.char_list[idx-4][int(p)]
            elif idx <= 7 :
                r_province = self.province_list[int(p)]
        return r_digit, r_char, r_province

    def _detection_to_recognition(self, image, detection_result, output_size=(224, 224) ):
        keypoint = detection_result['box']
        PAD = 2
        h, w = image.shape[:2]
        point_1 = (int(keypoint[1] * w - PAD), int(keypoint[0] * h - PAD))
        point_2 = (int(keypoint[3] * w + PAD), int(keypoint[2] * h + PAD))
        crop_image = image[point_1[1]:point_2[1],point_1[0]:point_2[0],:].copy()
        return crop_image, cv2.resize(crop_image, output_size)

    def destroy(self):
        self.detection_ai.destroy()
