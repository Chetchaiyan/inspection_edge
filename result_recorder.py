import datetime
import os
from PIL import Image
import csv

# ToDo : Move to somewhere safe
CSV_PATH = 'csv_record'
CROP_IMAGE_PATH = 'crop_image'
FULL_IMAGE_PATH = 'full_image'

class CsvRecorder:

    def __init__(self, config={}):
        self.csv_path = CSV_PATH
        self.crop_image_path = CROP_IMAGE_PATH
        self.full_image_path = FULL_IMAGE_PATH
        if 'csv_recorder' in config :
            self.csv_path = config['csv_recorder']['csv_path']
            self.crop_image_path = config['csv_recorder']['crop_image_path']
            self.full_image_path = config['csv_recorder']['full_image_path']

    def record(self, data, crop_image, full_image):
        timestamp = datetime.datetime.now()
        time_full = timestamp.strftime("%Y%m%d_%H%M%S_%f")
        time_hour = timestamp.strftime("%Y%m%d_%H")
        if not os.path.exists(self.csv_path) :
            os.mkdir(self.csv_path)
        if not os.path.exists(self.crop_image_path) :
            os.mkdir(self.crop_image_path)
        if not os.path.exists(self.full_image_path) :
            os.mkdir(self.full_image_path)
        if not os.path.exists(self.crop_image_path + "/" + time_hour) :
            os.mkdir(self.crop_image_path + "/" + time_hour)
        if not os.path.exists(self.full_image_path + "/" + time_hour) :
            os.mkdir(self.full_image_path + "/" + time_hour)
        img = Image.fromarray(crop_image)
        img.save(self.crop_image_path + "/" + time_hour + "/" + time_full + ".jpg", 'JPEG')
        img = Image.fromarray(full_image)
        img.save(self.full_image_path + "/" + time_hour + "/" + time_full + ".jpg", 'JPEG')
        with open(self.csv_path + "/" + time_hour + ".csv", "a") as csv_file :
            csv_writer = csv.writer(csv_file)
            csv_writer.writerow([time_full, str(data)])
