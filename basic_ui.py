import cv2

from tkinter import *
from PIL import Image, ImageTk

import chet_helper as helper

class BinaryClassificationResultUI(Frame):

    def __init__(self, result, image, master=None):
        super().__init__(master, borderwidth=1, relief="solid")
        self.result = result
        self.image = image
        self.create_widget()

    def create_widget(self):
        img = helper.fit_image(self.image, (128, 128))
        h, w = img.shape[:2]
        b_x = int((128 - w) / 2)
        b_y = int((128 - h) / 2)
        img = cv2.copyMakeBorder( img, b_y, b_y, b_x, b_x, cv2.BORDER_CONSTANT, (0, 0, 0))
        img = Image.fromarray(img)
        i = ImageTk.PhotoImage(img)
        self.label_image = Label(self, image=i)
        self.label_image.image = i
        self.label_image.grid(row=0, column=0, rowspan=2, sticky=N+E+W, padx=5, pady=5)
        self.label_text = StringVar()
        if self.result[0] > 0.5 :
            self.label_text.set("PASS")
            bg_color = '#3F3'
        else :
            self.label_text.set("FAIL")
            bg_color = '#F33'
        self.result_text = StringVar()
        self.result_text.set("Confident : {:.2f}".format(self.result[0]))
        self.label_1 = Label(self, textvariable=self.label_text, bg=bg_color, fg="#000", font=(None, 28))
        self.label_1.grid(row=0, column=1, sticky=N+E+W, padx=5, pady=5, ipadx=20, ipady=10)
        self.label_1 = Label(self, textvariable=self.result_text)
        self.label_1.grid(row=1, column=1, padx=5, pady=5)
