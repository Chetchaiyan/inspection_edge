from PIL import Image
from PIL import ImageTk

import numpy as np
from tkinter import filedialog
import tkinter as tki
import argparse
import uuid
import random
import string
import pickle
import os
import cv2
import glob
import datetime

PREFERED_RESOLUTION = [ (640,480), (1024,768), (1280,1024), (1920, 1080) ]
IMAGE_WIDGET_WIDTH = 800

class GlobalData(object):
    camera_id = 0
    camera_size = (640, 480)
    captured_image = None
    defect_string = None
    display_image = None
    info_label = None
    marking_size = 8
    overlay_image = None
    webcam = None
    webcam_frame = None
    widget_size = (800, 450)
    latest_filename = ''
    view_image_frame = None
    storage_path = ''
    lview = None

def local_inference(sub_image, f_graph_file="coating_mobilenet_optimized_model.pb"):
    import tensorflow as tf
    with tf.gfile.GFile(f_graph_file, "rb") as f:
        restored_graph_def = tf.GraphDef()
        restored_graph_def.ParseFromString(f.read())
    with tf.Graph().as_default() as graph:
        cnn_input, softmax = tf.import_graph_def( restored_graph_def, input_map=None,
                     return_elements=['cnn_input:0', 'softmax:0'], name="")
    sess= tf.Session(graph=graph)
    feed_dict_testing = {cnn_input: sub_image}
    result=sess.run(softmax, feed_dict=feed_dict_testing)
    return result

def draw_softmax(image, softmax, cutoff=0.01):
    num_row = image.shape[1] // 128
    num_col = image.shape[0] // 128
    counter = 0
    for each_softmax in softmax :
        x = (counter // num_col * 128) + 2
        y = (counter % num_col * 128) + 2
        if each_softmax[0] < cutoff :
            cv2.rectangle(image,(x,y),(x+124,y+124),(255,0,0),4)
        counter += 1

def inference(image):
    num_row = image.shape[1] // 128
    num_col = image.shape[0] // 128
    temp_softmax = np.zeros([num_row*num_col, 2])
    sub_image = []
    for row in range(num_row):
        for col in range(num_col):
            start_x = row * 128
            start_y = col * 128
            sub_image.append( image[start_y:start_y+128, start_x:start_x+128, :] )
    softmax = local_inference(sub_image)
    return draw_softmax(image, softmax)

def set_res(cap, x,y):
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, int(x))
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(y))
    return cap.get(cv2.CAP_PROP_FRAME_WIDTH) , cap.get(cv2.CAP_PROP_FRAME_HEIGHT)

def update_info_label(g):
    info_text = 'Camera : {}\nResolution : {} x {}\nStorage Path : {}\nLatest File : \n{}'.format(g.camera_id,
                int(g.camera_size[0]), int(g.camera_size[1]), g.storage_path, g.latest_filename )
    g.info_label.configure(text=info_text)

def save_image(g):
    if not os.path.exists(g.storage_path):
        os.makedirs(g.storage_path)
    g.latest_filename = str(uuid.uuid1()) + "-" + ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(8))
    write_image = cv2.cvtColor(g.captured_image, cv2.COLOR_RGB2BGR)
    cv2.imwrite("{}/{}.jpg".format(g.storage_path, g.latest_filename), write_image)

def retake(g):
    g.view_image_frame.grid_forget()
    g.webcam_frame.grid(row=0)
    save_image(g)

def capture(g):
    g.webcam_frame.grid_forget()
    _, frame = g.webcam.read()
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    g.captured_image = cv2image
    g.overlay_image = inference(cv2image)
    g.display_image = cv2image.copy()
    display_image_to_tk(cv2image, g.lview, g.widget_size)
    g.view_image_frame.grid(row=0)

def display_image_to_tk(image, tk_label, size):
    resized_image = cv2.resize(image, size)
    img = Image.fromarray(resized_image)
    imgtk = ImageTk.PhotoImage(image=img)
    tk_label.imgtk = imgtk
    tk_label.configure(image=imgtk)

def show_webcam_frame(g, lmain):
    _, frame = g.webcam.read()
    cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    display_image_to_tk(cv2image, lmain, g.widget_size)
    lmain.after(5, lambda g=g, lmain=lmain: show_webcam_frame(g, lmain) )

def main():
    parser = argparse.ArgumentParser(prog='PROG', description='Visual inspect and inference at manufacturing.')
    parser.add_argument('-c', help='specific camera id/name using for collecting data', default='0')
    parser.add_argument('-r', help='specific camera resolution using for collecting data', nargs=2, type=int)
    args = parser.parse_args()

    global_data = GlobalData()
    global_data.camera_id = args.c if len(args.c) > 2 else int(args.c)
    root = tki.Tk()
    global_data.webcam_frame = tki.Frame(root)
    global_data.webcam = cv2.VideoCapture(global_data.camera_id)
    if args.r :
        global_data.camera_size = set_res(global_data.webcam, args.r[0], args.r[1])
    else :
        for i in PREFERED_RESOLUTION :
            global_data.camera_size = set_res(global_data.webcam, i[0], i[1])
    global_data.storage_path = datetime.date.today().isoformat()
    widget_height = int( global_data.camera_size[1] / global_data.camera_size[0] * IMAGE_WIDGET_WIDTH )
    global_data.widget_size = (IMAGE_WIDGET_WIDTH, widget_height)
    global_data.info_label = tki.Label(global_data.webcam_frame, anchor=tki.W, justify=tki.LEFT)
    update_info_label(global_data)
    global_data.info_label.grid(row=0, column=1, sticky=tki.W+tki.N+tki.E, padx=5, pady=5)
    lmain = tki.Label(global_data.webcam_frame)
    lmain.configure(width=global_data.widget_size[0], height=global_data.widget_size[1])
    lmain.grid(row=0, rowspan=2, sticky=tki.W, padx=5, pady=5)
    global_data.webcam_frame.grid(row=0)
    global_data.webcam_frame.grid_columnconfigure(1, minsize=300)

    capture_button = tki.Button(global_data.webcam_frame, text='Capture', command=lambda g=global_data: capture(g) )
    capture_button.grid(row=1, column=1, sticky=tki.E+tki.S+tki.W, padx=5, pady=5, ipadx=10, ipady=5)
    global_data.view_image_frame = tki.Frame(root)
    global_data.lview = tki.Label(global_data.view_image_frame)
    global_data.lview.configure(width=global_data.widget_size[0], height=global_data.widget_size[1])
    global_data.lview.grid(row=0, padx=5, pady=5)
    back_button = tki.Button(global_data.view_image_frame, text='Back', command=lambda g=global_data: retake(g) )
    back_button.grid(row=0, column=1, sticky=tki.W+tki.E+tki.S, padx=5, pady=5, ipadx=10, ipady=5)

    root.title("PanyaPradit - Collecting Data V1.0")
    root.geometry("1120x{}".format(global_data.widget_size[1] + 20) )
    root.resizable(0,0)

    show_webcam_frame(global_data, lmain)
    root.mainloop()

if __name__ == "__main__" :
    main()
