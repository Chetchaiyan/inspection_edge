import numpy as np
import tensorflow.compat.v1 as tf_v1
import tensorflow as tf

class BaseTensorFlowAI:

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.input_tensor = input_tensor
        self.output_tensor = output_tensor
        self.sess = None

    def create_graph(self, frozen_graph_file):
        self.graph = tf_v1.Graph()
        with self.graph.as_default():
            od_graph_def = tf_v1.GraphDef()
            with tf_v1.gfile.GFile(frozen_graph_file, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf_v1.import_graph_def(od_graph_def, name='')
            self.sess = tf_v1.Session()
            ops = tf_v1.get_default_graph().get_operations()
            all_tensor_names = {output.name for op in ops for output in op.outputs}
            self.tensor_dict = {}
            for key in self.output_tensor:
                tensor_name = key + ':0'
                if tensor_name in all_tensor_names:
                    self.tensor_dict[key] = tf_v1.get_default_graph().get_tensor_by_name(tensor_name)

    def destroy(self):
        if self.sess :
            self.sess.close()

    def inference(self, input):
        with self.graph.as_default():
            feed_dict = {}
            for i in self.input_tensor :
                feed_dict[tf_v1.get_default_graph().get_tensor_by_name(i + ':0')] = input[i]
            output_dict = self.sess.run(self.tensor_dict, feed_dict=feed_dict)
        return self._process_output( output_dict )

    def _process_output(self, output_dict):
        return output_dict

class BaseTensorKerasAI:

    def __init__(self, input_tensor=[], output_tensor=[]):
        self.model = None

    def load_model(self, model_folder):
        self.model = tf.saved_model.load(model_folder)

    def destroy(self):
        pass

    def inference(self, input):
        input = self._preprocess_input(input)
        pred = self.model( input )
        return pred[0]

    def _preprocess_input(self, input):
        return input

    def _process_output(self, pred):
        return pred

class BaseComplexModel:

    def __init__(self):
        pass

    def load_model(self):
        pass

    def destroy(self):
        pass

    def inference(self, input):
        pass

    def _process_output(self, pred):
        return pred
